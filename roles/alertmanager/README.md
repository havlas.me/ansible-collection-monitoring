havlasme.monitoring.alertmanager
================================

[![Apache-2.0 license][license-image]][license-link]

An [Ansible](https://www.ansible.com/) role to manage [alertmanager](https://prometheus.io/docs/alerting/latest/alertmanager/) service.

Requirements
------------

None.

Role Variables
--------------

```yaml
# start the alertmanager service at system boot
alertmanager_service_enabled: "yes"
# can ansible restart the alertmanager service
alertmanager_service_restart: "yes"

# the alertmanager release version
alertmanager_release_tag: "latest"
# the alertmanager release download url
# yamllint disable-line rule:line-length
#alertmanager_release_url: "https://github.com/prometheus/alertmanager/releases/download/v{{ alertmanager_release_tag }}/alertmanager-{{ alertmanager_release_tag }}.linux-{{ 'amd64' if ansible_architecture == 'x86_64' else ansible_architecture }}.tar.gz"

# the alertmanager service user
alertmanager_runas_user: "alertmanager"
# the alertmanager service group
alertmanager_runas_group: "alertmanager"

# the alertmanager config directory
alertmanager_confdir: "/etc/alertmanager"
# the alertmanager data directory
alertmanager_datadir: "/var/lib/alertmanager"

# the alertmanager main configuration
alertmanager_conf_main: "{{ alertmanager_confdir }}/alertmanager.yml"
# the alertmanager main configuration template
alertmanager_conf_tmpl: "alertmanager.yml.j2"
# the alertmanager raw yaml configuration
#alertmanager_conf_yaml:

# the alertmanager external url
alertmanager_external_url: ""
# the alertmanager listen host
alertmanager_network_bind: "127.0.0.1"
# the alertmanager listen port
alertmanager_network_port: "9093"
# the alertmanager data retention time
alertmanager_retention_time: "120h"

# the alertmanager service option
alertmanager_service_option:
- "--config.file={{ alertmanager_conf_main }}"
- "--cluster.listen-address="
- "--data.retention={{ alertmanager_retention_time }}"
- "--storage.path={{ alertmanager_datadir }}"
- "--web.external-url={{ alertmanager_external_url }}"
- "--web.listen-address={{ alertmanager_network_bind }}:{{ alertmanager_network_port }}"
```

Dependencies
------------

None.

Example Playbook
----------------

```yaml
- hosts: all
  tasks:
  - import_role:
      name: havlasme.monitoring.alertmanager
```

License
-------

Apache-2.0

Author Information
------------------

Created by [Tomáš Havlas](https://havlas.me/).

[license-image]: https://img.shields.io/badge/license-Apache2.0-blue.svg?style=flat-square
[license-link]: ../../LICENSE
