Ansible Role - havlasme.monitoring.mysqld_exporter
==================================================

[![Apache-2.0 license][license-image]][license-link]

An [Ansible](https://www.ansible.com/) role to install and configure [mysqld exporter](https://github.com/prometheus/mysqld_exporter) service on [Debian](https://www.debian.org/) and [Ubuntu](https://ubuntu.com/).

Role Variables
--------------

```yaml
# start the mysqld exporter service at system boot
mysqld_exporter_service_enabled: "yes"
# can ansible restart the mysqld exporter service
mysqld_exporter_service_restart: "yes"

# the mysqld exporter release version
mysqld_exporter_release_tag: "latest"
# the mysqld exporter release download url
# yamllint disable-line rule:line-length
#mysqld_exporter_release_url: "https://github.com/prometheus/mysqld_exporter/releases/download/v{{ mysqld_exporter_release_url }}/mysqld_exporter-{{ mysqld_exporter_release_url }}.linux-{{ 'amd64' if ansible_architecture == 'x86_64' else ansible_architecture }}.tar.gz"

# the mysqld exporter service user
mysqld_exporter_runas_user: "mysqld_exporter"
# the mysqld exporter service group
mysqld_exporter_runas_group: "mysqld_exporter"

# the mysqld exporter datasource
#mysqld_exporter_datasource: ""

# the mysqld exporter network bind ip
mysqld_exporter_network_bind: ""
# the mysqld exporter network port
mysqld_exporter_network_port: "9104"

# the mysqld exporter service option
mysqld_exporter_service_option:
- "--web.listen-address={{ mysqld_exporter_network_bind }}:{{ mysqld_exporter_network_port }}"
```

Example Playbook
----------------

```yaml
- hosts: 'all'
  tasks:
  - ansible.builtin.include_role:
      name: 'havlasme.monitoring.mysqld_exporter'
```

License
-------

[Apache-2.0][license-link]

Author Information
------------------

Created by [Tomáš Havlas](https://havlas.me/).


[license-image]: https://img.shields.io/badge/license-Apache2.0-blue.svg?style=flat-square
[license-link]: ../../LICENSE
