Ansible Role - havlasme.monitoring.apt_collector
================================================

[![Apache-2.0 license][license-image]][license-link]

An [Ansible](https://www.ansible.com/) role to install and configure the [apt collector script](https://github.com/prometheus-community/node-exporter-textfile-collector-scripts/) with the node exporter on [Debian](https://www.debian.org/) and [Ubuntu](https://ubuntu.com/).

Role Variables
--------------

```yaml
# the apt collector state ('present', 'latest', 'absent')
apt_collector_state: 'present'
```

Example Playbook
----------------

```yaml
- hosts: 'all'
  
  tasks:
  - ansible.builtin.include_role:
      name: 'havlasme.monitoring.apt_collector'
```

License
-------

[Apache-2.0][license-link]

Author Information
------------------

Created by [Tomáš Havlas](https://havlas.me/).


[license-image]: https://img.shields.io/badge/license-Apache2.0-blue.svg?style=flat-square
[license-link]: ../../LICENSE
