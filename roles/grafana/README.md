havlasme.monitoring.grafana
===========================

[![Apache-2.0 license][license-image]][license-link]

An [Ansible](https://www.ansible.com/) role to manage grafana service.

Requirements
------------

None.

Role Variables
--------------

```yaml
# the grafana package state ('present', 'latest')
grafana_state: "present"
# start the grafana service at system boot
grafana_service_enabled: "yes"
# can ansible restart the grafana service
grafana_service_restart: "yes"

# the grafana repository key url
grafana_repo_key_url:
- "https://packages.grafana.com/gpg.key"
# the grafana repository url
grafana_repo_url: "deb https://packages.grafana.com/enterprise/deb stable main"
```

Dependencies
------------

None.

Example Playbook
----------------

```yaml
- hosts: all
  tasks:
  - import_role:
      name: havlasme.monitoring.grafana
```

License
-------

Apache-2.0

Author Information
------------------

Created by [Tomáš Havlas](https://havlas.me/).

[license-image]: https://img.shields.io/badge/license-Apache2.0-blue.svg?style=flat-square
[license-link]: ../../LICENSE
