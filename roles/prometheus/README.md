havlasme.monitoring.prometheus
==============================

[![Apache-2.0 license][license-image]][license-link]

An [Ansible](https://www.ansible.com/) role to manage prometheus service.

Requirements
------------

None.

Role Variables
--------------

```yaml
# start the prometheus service at system boot
prometheus_service_enabled: "yes"
# can ansible restart the prometheus service
prometheus_service_restart: "yes"

# the prometheus release version
prometheus_release_tag: "latest"
# the prometheus release download url
# yamllint disable-line rule:line-length
#prometheus_release_url: "https://github.com/prometheus/prometheus/releases/download/v{{ prometheus_release_tag }}/prometheus-{{ prometheus_release_tag }}.linux-{{ 'amd64' if ansible_architecture == 'x86_64' else ansible_architecture }}.tar.gz"

# the prometheus service user
prometheus_runas_user: "prometheus"
# the prometheus service group
prometheus_runas_group: "prometheus"

# the prometheus configuration directory
prometheus_confdir: "/etc/prometheus"
# the prometheus data directory
prometheus_datadir: "/var/lib/prometheus"

# the prometheus main configuration
prometheus_conf_main: "{{ prometheus_confdir }}/prometheus.yml"
# the prometheus main configuration template
prometheus_conf_tmpl: "prometheus.yml.j2"
# the prometheus raw yaml configuration
#prometheus_conf_yaml:
# the prometheus ruleset
prometheus_ruleset: []
# - dest: string
#   src: string | d('ruleset.rules.j2')
#   state: enum('present', 'absent') | d('present')

# the prometheus external url
prometheus_external_url: ""
# the prometheus network bind ip
prometheus_network_bind: "127.0.0.1"
# the prometheus network port
prometheus_network_port: "9090"
# the prometheus data retention time
prometheus_retention_time: "15d"
# the prometheus data retention size
prometheus_retention_size: "0"

# the prometheus service option
prometheus_service_option:
- "--config.file={{ prometheus_conf_main }}"
- "--storage.tsdb.path={{ prometheus_datadir }}"
- "--storage.tsdb.retention.time={{ prometheus_retention_time }}"
- "--storage.tsdb.retention.size={{ prometheus_retention_size }}"
- "--web.console.templates={{ prometheus_confdir }}/console"
- "--web.console.libraries={{ prometheus_confdir }}/console_library"
- "--web.external-url={{ prometheus_external_url }}"
- "--web.listen-address={{ prometheus_network_bind }}:{{ prometheus_network_port }}"
```

Dependencies
------------

None.

Example Playbook
----------------

```yaml
- hosts: all
  tasks:
  - import_role:
      name: havlasme.monitoring.prometheus
```

License
-------

Apache-2.0

Author Information
------------------

Created by [Tomáš Havlas](https://havlas.me/).

[license-image]: https://img.shields.io/badge/license-Apache2.0-blue.svg?style=flat-square
[license-link]: ../../LICENSE
