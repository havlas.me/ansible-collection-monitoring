havlasme.monitoring.json_exporter
=================================

[![Apache-2.0 license][license-image]][license-link]

An [Ansible](https://www.ansible.com/) role to manage json exporter service.

Requirements
------------

None.

Role Variables
--------------

```yaml
# the json exporter release version
json_exporter_release_tag: "latest"
# the json exporter release download url
#json_exporter_release_url: "https://github.com/prometheus-community/json_exporter/releases/download/v{{ json_exporter_release_tag }}/json_exporter-{{ json_exporter_release_tag }}.linux-{{ 'amd64' if ansible_architecture == 'x86_64' else ansible_architecture }}.tar.gz"

# the json exporter service user
json_exporter_runas_user: "json_exporter"
# the json exporter service group
json_exporter_runas_group: "json_exporter"
# the json exporter config directory
json_exporter_confdir: "/etc/json_exporter"

# the json exporter main configuration
#json_exporter_conf_main: "{{ json_exporter_confdir }}/example.yml"
# the json exporter main configuration template
#json_exporter_conf_tmpl: "example.yml.j2"

# the json exporter service option
json_exporter_service_option:
- "--port=7979"
```

Dependencies
------------

None.

Example Playbook
----------------

```yaml
- hosts: all
  tasks:
  - import_role:
      name: havlasme.monitoring.json_exporter
```

License
-------

Apache-2.0

Author Information
------------------

Created by [Tomáš Havlas](https://havlas.me/).

[license-image]: https://img.shields.io/badge/license-Apache2.0-blue.svg?style=flat-square
[license-link]: ../../LICENSE
