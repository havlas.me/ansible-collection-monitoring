Ansible Role - havlasme.monitoring.node_exporter
================================================

[![Apache-2.0 license][license-image]][license-link]

An [Ansible](https://www.ansible.com/) role to install and configure the [node exporter](https://github.com/prometheus/node_exporter) service on [Debian](https://www.debian.org/) and [Ubuntu](https://ubuntu.com/).

Role Variables
--------------

```yaml
# the node exporter package state ('present', 'latest', 'absent')
node_exporter_state: 'present'
# should the node exporter service start at boot
node_exporter_enabled: '{{ node_exporter_state != "absent" }}'
# can ansible restart the node exporter service?
node_exporter_ansible_restart: true

# the node exporter release version
# @see https://github.com/prometheus/node_exporter/releases/
node_exporter_release_tag: 'latest'
# the node exporter release download url
# yamllint disable-line rule:line-length
#node_exporter_release_url: "https://github.com/prometheus/node_exporter/releases/download/v{{ node_exporter_release_tag }}/node_exporter-{{ node_exporter_release_tag }}.linux-{{ 'amd64' if ansible_architecture == 'x86_64' else ansible_architecture }}.tar.gz"

# the node exporter listen port
node_exporter_port: 9100
# the node exporter listen ip
#node_exporter_listen_to: '0.0.0.0'

# the node exporter service user
node_exporter_runas_user: 'node_exporter'
# the node exporter service group
node_exporter_runas_group: 'node_exporter'

# the node exporter data directory
node_exporter_datadir: '/var/lib/node_exporter'

# the node exporter textfile group
node_exporter_textfile_group: '{{ node_exporter_runas_group }}'
# the node exporter textfile directory
node_exporter_textfile_d: '{{ node_exporter_datadir }}/textfile_collector'
# mount a ramdisk at the node textfile collector directory?
node_exporter_textfile_ramdisk: true

# the node exporter collector configuration
node_exporter_collector:
- textfile:
    directory: '{{ node_exporter_textfile_d }}'

# the node exporter service configuration
node_exporter_service_conf:
- '--web.listen-address={{ node_exporter_listen_to | d("") }}:{{ node_exporter_port }}'
```

Example Playbook
----------------

```yaml
- hosts: 'all'
  
  tasks:
  - ansible.builtin.include_role:
      name: 'havlasme.monitoring.node_exporter'
```

License
-------

[Apache-2.0][license-link]

Author Information
------------------

Created by [Tomáš Havlas](https://havlas.me/).


[license-image]: https://img.shields.io/badge/license-Apache2.0-blue.svg?style=flat-square
[license-link]: ../../LICENSE
