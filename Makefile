NAMESPACE = $(shell yq -r .namespace galaxy.yml)
ROLENAME = $(shell yq -r .name galaxy.yml)
ROLEVERSION = $(shell yq -r .version galaxy.yml)
DIST = ./dist
GALAXY = @ansible-galaxy

.PHONY: build
build:
	$(GALAXY) collection build --output-path "$(DIST)"

.PHONY: clean
clean:
	-rm --recursive "$(DIST)"

.PHONY: install
install:
ifeq (, $(shell which yq))
	$(error "no yq. try doing pip3 install yq")
else
	$(GALAXY) collection install "$(DIST)/$(NAMESPACE)-$(ROLENAME)-$(ROLEVERSION).tar.gz"
endif

.PHONY: lint
lint:
	$(MAKE) -C roles/alertmanager lint
	$(MAKE) -C roles/blackbox_exporter lint
	$(MAKE) -C roles/export_node_apt lint
	$(MAKE) -C roles/grafana lint
	$(MAKE) -C roles/json_exporter lint
	$(MAKE) -C roles/mysqld_exporter lint
	$(MAKE) -C roles/nftables_exporter lint
	$(MAKE) -C roles/node_exporter lint
	$(MAKE) -C roles/prometheus lint
	$(MAKE) -C roles/snmp_exporter lint

.PHONY: test
test:
	$(MAKE) -C roles/alertmanager test
	$(MAKE) -C roles/blackbox_exporter test
	$(MAKE) -C roles/export_node_apt test
	$(MAKE) -C roles/grafana test
	$(MAKE) -C roles/json_exporter test
	$(MAKE) -C roles/mysqld_exporter test
	$(MAKE) -C roles/nftables_exporter test
	$(MAKE) -C roles/node_exporter test
	$(MAKE) -C roles/prometheus test
	$(MAKE) -C roles/snmp_exporter test
