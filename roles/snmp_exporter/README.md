havlasme.monitoring.snmp_exporter
=================================

[![Apache-2.0 license][license-image]][license-link]

An [Ansible](https://www.ansible.com/) role to manage snmp exporter service.

Requirements
------------

None.

Role Variables
--------------

```yaml
# the snmp exporter release version
snmp_exporter_release_tag: "latest"
# the snmp exporter release download url
# yamllint disable-line rule:line-length
#snmp_exporter_release_url: "https://github.com/prometheus/snmp_exporter/releases/download/v{{ snmp_exporter_release_tag }}/snmp_exporter-{{ snmp_exporter_release_tag }}.linux-{{ 'amd64' if ansible_architecture == 'x86_64' else ansible_architecture }}.tar.gz"

# the snmp exporter service user
snmp_exporter_runas_user: "snmp_exporter"
# the snmp exporter service group
snmp_exporter_runas_group: "snmp_exporter"
# the snmp exporter config directory
snmp_exporter_confdir: "/etc/snmp_exporter"

# the snmp exporter main configuration
snmp_exporter_conf_main: "{{ snmp_exporter_confdir }}/snmp.yml"
# the snmp exporter main configuration template
#snmp_exporter_conf_tmpl: "snmp.yml.j2"

# the snmp exporter listen host
snmp_exporter_network_bind: "127.0.0.1"
# the snmp exporter listen port
snmp_exporter_network_port: "9116"

# the snmp exporter service version
snmp_exporter_service_option:
- "--web.listen-address={{ snmp_exporter_network_bind }}:{{ snmp_exporter_network_port }}"
```

Dependencies
------------

None.

Example Playbook
----------------

```yaml
- hosts: all
  tasks:
  - import_role:
      name: havlasme.monitoring.snmp_exporter
```

License
-------

Apache-2.0

Author Information
------------------

Created by [Tomáš Havlas](https://havlas.me/).

[license-image]: https://img.shields.io/badge/license-Apache2.0-blue.svg?style=flat-square
[license-link]: ../../LICENSE
