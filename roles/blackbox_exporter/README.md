havlasme.monitoring.blackbox_exporter
=====================================

[![Apache-2.0 license][license-image]][license-link]

An [Ansible](https://www.ansible.com/) role to manage blackbox exporter service.

Requirements
------------

None.

Role Variables
--------------

```yaml
# start the blackbox exporter service at system boot
blackbox_exporter_service_enabled: "yes"
# can ansible restart the blackbox exporter service
blackbox_exporter_service_restart: "yes"

# the blackbox exporter release version
blackbox_exporter_release_tag: "latest"
# the blackbox exporter release download url
# yamllint disable-line rule:line-length
#blackbox_exporter_release_url: "https://github.com/prometheus/blackbox_exporter/releases/download/v{{ blackbox_exporter_release_tag }}/blackbox_exporter-{{ blackbox_exporter_release_tag }}.linux-{{ 'amd64' if ansible_architecture == 'x86_64' else ansible_architecture }}.tar.gz"

# the blackbox exporter service user
blackbox_exporter_runas_user: "blackbox_exporter"
# the blackbox exporter service group
blackbox_exporter_runas_group: "blackbox_exporter"
# the blackbox exporter config directory
blackbox_exporter_confdir: "/etc/blackbox_exporter"

# the blackbox exporter main configuration
blackbox_exporter_conf_main: "{{ blackbox_exporter_confdir }}/blackbox.yml"
# the blackbox exporter main configuration template
blackbox_exporter_conf_tmpl: "blackbox.yml.j2"
# the blackbox exporter raw yaml configuration
#blackbox_exporter_conf_yaml:

# the blackbox exporter network bind ip
blackbox_exporter_network_bind: "127.0.0.1"
# the blackbox exporter network port
blackbox_exporter_network_port: "9115"

# the blackbox exporter service option
blackbox_exporter_service_option:
- "--config.file={{ blackbox_exporter_conf_main }}"
- "--web.listen-address={{ blackbox_exporter_network_bind }}:{{ blackbox_exporter_network_port }}"
```

Dependencies
------------

[Community.General](https://docs.ansible.com/ansible/latest/collections/community/general/index.html)

```bash
ansible-galaxy collection install community.general
```

Example Playbook
----------------

```yaml
- hosts: all
  tasks:
  - import_role:
      name: havlasme.monitoring.blackbox_exporter
```

License
-------

Apache-2.0

Author Information
------------------

Created by [Tomáš Havlas](https://havlas.me/).

[license-image]: https://img.shields.io/badge/license-Apache2.0-blue.svg?style=flat-square
[license-link]: ../../LICENSE
