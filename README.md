Ansible Collection - havlasme.monitoring
========================================

[![Apache-2.0 license][license-image]][license-link]

An [Ansible](https://www.ansible.com/) collection of linux host and service monitoring content that supports [Debian](https://www.debian.org/) and [Ubuntu](https://ubuntu.com/).

### Included Content

- [havlasme.monitoring.alertmanager](/roles/alertmanager/README.md)
- [havlasme.monitoring.apt_collector](/roles/apt_collector/README.md)
- [havlasme.monitoring.blackbox_exporter](/roles/blackbox_exporter/README.md)
- [havlasme.monitoring.grafana](/roles/grafana/README.md)
- [havlasme.monitoring.json_exporter](/roles/json_exporter/README.md)
- [havlasme.monitoring.mysqld_exporter](/roles/mysqld_exporter/README.md)
- [havlasme.monitoring.node_exporter](/roles/node_exporter/README.md)
- [havlasme.monitoring.prometheus](/roles/prometheus/README.md)
- [havlasme.monitoring.snmp_exporter](/roles/snmp_exporter/README.md)

Installation
------------

```bash
ansible-galaxy collection install havlasme.monitoring
```

```yaml title="requirements.yml"
---
collections:
- name: 'havlasme.monitoring'
...
```

Development
-----------

```shell
make build
```

```shell title="ansible-lint"
make lint
```

```shell title="molecule"
make test
```

License
-------

[Apache-2.0][license-link]

Author Information
------------------

Created in 2024 by [Tomáš Havlas](https://havlas.me/).


[license-image]: https://img.shields.io/badge/license-Apache2.0-blue.svg?style=flat-square
[license-link]: LICENSE
